import {useState, useContext} from 'react';

import{Link, NavLink} from 'react-router-dom';
import {Col, Row, Container} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import UserContext from '../UserContext';

// import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
// import NavDropdown from 'react-bootstrap/NavDropdown';

export default function AppNavbar() {

  const { user } = useContext(UserContext);

  const [cartQuantity, setCartQuantity] = useState(0)

console.log(user)


  return (
    <>
    <Navbar sticky="top" bg="light" expand="lg" className="shadow-sm mb-3">
      {/*<Container>*/}
        <Navbar.Brand as={Link} to="/" >
          <img
              src="/images/logo.png"
              width="150"
              height="50"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />

        </Navbar.Brand>

        {(user.isAdmin)?
        <Navbar.Brand > Hello Admin</Navbar.Brand>
        
        :
        <>
        {(user.id !== null)?
        <Navbar.Brand as={Link} to="/details"> Hello {user.first}</Navbar.Brand>
        :
        ""
        }</>
        }

{/*            <img
              src="./logo1.jpg"
              width="30"
              height="30"
              className="d-inline-block align-top"
              alt="React Bootstrap logo"
            />*/}



        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
          <Nav className="justify-content-end">


              {(user.isAdmin)?
              <>
              <Nav.Link as={NavLink} to="/admin">Admin Dashboard</Nav.Link>
              {/*<Nav.Link as={NavLink} to="/products">All Products</Nav.Link>
              <Nav.Link as={NavLink} to="/addProduct">Create Product</Nav.Link>*/}
              </>
              :
              <>
              
              <Nav.Link as={NavLink} to="/cart" style={{ width: "3rem", height: "3rem", position: "relative" }}>
              

              <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 576 512"
              fill="currentColor"
              >
              <path d="M96 0C107.5 0 117.4 8.19 119.6 19.51L121.1 32H541.8C562.1 32 578.3 52.25 572.6 72.66L518.6 264.7C514.7 278.5 502.1 288 487.8 288H170.7L179.9 336H488C501.3 336 512 346.7 512 360C512 373.3 501.3 384 488 384H159.1C148.5 384 138.6 375.8 136.4 364.5L76.14 48H24C10.75 48 0 37.25 0 24C0 10.75 10.75 0 24 0H96zM128 464C128 437.5 149.5 416 176 416C202.5 416 224 437.5 224 464C224 490.5 202.5 512 176 512C149.5 512 128 490.5 128 464zM512 464C512 490.5 490.5 512 464 512C437.5 512 416 490.5 416 464C416 437.5 437.5 416 464 416C490.5 416 512 437.5 512 464z" />
              </svg>

              <div className="rounded-circle bg-secondary d-flex justify-content-center align-items-center" style={{color: "white", width: "1.5rem", height: "1.5rem", position: "absolute", bottom: 0, right: 0, transform: "translate(25%, 25%",}}>{cartQuantity}</div>

              </Nav.Link>
              <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
              </>

              }              

              { (user.id !== null) ?
              <Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
              :
              <>              
              
              <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
              </>
              }




{/*            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            <Nav.Link href="#link">Login</Nav.Link>*/}
{/*            <NavDropdown title="Dropdown" id="basic-nav-dropdown">
              <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">
                Another action
              </NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
              <NavDropdown.Item href="#action/3.4">
                Separated link
              </NavDropdown.Item>
            </NavDropdown>*/}
          </Nav>
        </Navbar.Collapse>
      {/*</Container>*/}
    </Navbar>
    
    
    
    

    
    

    
    

    </>
  );
}
