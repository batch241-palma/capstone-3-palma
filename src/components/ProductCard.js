import {useState, useEffect, useContext} from 'react';

import {Link} from 'react-router-dom';

import {Card, Button, ToggleButton, Container, Row, Col} from 'react-bootstrap';

import UserContext from '../UserContext';
import ProductProvider from '../ProductContext';
	
export default function ProductCard({product}) {

	const {name, description, price, _id, isActive, quantity} = product;
  const [state, setState] = useState(product.isActive)
  const [cartQuantity, setCartQuantity] = useState(0)
  const [userproduct, setuserProduct] = useState({
    quantity: null
  })
  console.log(product.isActive)

  const toggle = () => {
    setState(!state);
  }


  function cartQuantityA() {
    
    setCartQuantity(cartQuantity + 1);
    setuserProduct({
      quantity: cartQuantity
    })
  }

  console.log(state)

  const { user } = useContext(UserContext);
  
  return (
  
 <>
  


 
<Col xs={12} md={6} lg={4} className="g-3">
  <Card >
      <Card.Img variant="top" src="/images/sample.jpg" />
{/*    <Card.Img variant="top" src={./images/logo.png} height="200px" style={{objectFit: "cover"}}>*/}
      <Card.Body className="text-center">
          <Card.Title>{name}</Card.Title>
          <Card.Subtitle>Description:</Card.Subtitle>
          <Card.Text>{description}</Card.Text>
          <Card.Subtitle>Price:</Card.Subtitle>
          <Card.Text>PhP {price}</Card.Text>
          {/*<Card.Text>Enrollees: {count} </Card.Text>*/}
          {/*<Button variant="primary">Enroll</Button>*/}
          {/*<Button className="bg-primary" onClick={enroll}>Enroll</Button>*/}

          <>
          <br/>
          {(user.isAdmin)?
          <>
          <Card.Subtitle className={isActive? "badge bg-primary text-wrap"  : "badge bg-danger text-wrap"}>{isActive? 'Active'  : 'Inactive'}</Card.Subtitle>
          <br/>
          <Button className="bg-success" as={Link} to={`/${_id}`}>Update</Button>      
          </>    
            :
          <>
          <div className="d-flex align-items-center justify-content-center" style={{gap: "2rem"}}>
          <Button className="bg-primary" as={Link} to={`/products/${_id}`}>Details</Button>
          <Button className="bg-primary" onClick ={cartQuantityA}>Add to cart</Button>
          </div>
          </>
          }
          <br/>
          </>
          
          
      </Card.Body>
  </Card>
</Col>
   
</>


	)
}