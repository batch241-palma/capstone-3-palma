import { useState, useEffect, useContext } from 'react';

import { Container, Card, Button, Row, Col } from 'react-bootstrap';

import {useParams, Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function ProductView() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// The "useParams" is a hook that allows us to retrieve the courseId passed via URL params
	const {productId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [minus, setMinus] = useState(0);
	const [add, setAdd] = useState();


	// Place Order
	const order = (productId) => {
		console.log(user)
		console.log(productId)
		fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
			method: "POST",
			headers: {
				'content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		
			if(data === true){
				Swal.fire({
	 			 title: "Order placed",
	 			 icon: "success",
	 			 text: "You have successfully added this product."
				})
				navigate("/products");
			} else {
	 		 	Swal.fire({
		 		title: "Something went wrong",
		  		icon: "error",
		 		text: "Please try again."
				})
			}	

		})
	}


	function addItem() {
		setQuantity(quantity + 1);
		console.log(quantity)
	}

	function minusItem() {
		if(quantity>0){
		setQuantity(quantity - 1);}
		console.log(quantity)
	}	


	// Retrieve product details
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId])

	return (



		<Container>
			<Row>
			<Button className="bg-dark align-items-center justify-content-center" as={Link} to={`/products`}>Return to product page</Button>
				<Col lg={{span: 6, offset:3}} >
					
					<br/>
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title className="bg-secondary text-light p-3 shadow-sm" style={{gap: ".5rem"}}>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        <Card.Subtitle>Quanity</Card.Subtitle>
					        <Card.Text>
					        
					        <div className = "d-flex align-items-center flex-column" style={{gap: ".5rem"}}>
					        <div className="d-flex align-items-center justify-content-center" style={{gap: ".5rem"}}>
					        	<Button className="bg-dark" onClick ={minusItem} >-</Button>
					        	<div>
					        		<span className="fs-3">{quantity}</span>
					        	</div>
					        	<Button className="bg-dark" onClick ={addItem}>+</Button>
					        	</div>
					        	<Button variant="danger">Remove</Button>
					        </div>
					        
					        </Card.Text>
					        
					        {
					        	(user.id !== null) ?
					        	<>
					        		{(quantity>0)? 
					        		<Button variant="primary" className="w=100" onClick={() => order(productId)} >Proceed to checkout</Button>
					        		:
					        		<Button variant="primary" className="w=100" onClick={() => order(productId)} disabled>Proceed to checkout</Button>
					        		}
					        		<br/>
					        		{/*<Button className="bg-primary" as={Link} to={`/products`}>Add to cart</Button>*/}
					        	</>
					        		:
					        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Order</Button>
					        }
					        <br/>
					       {/* <Button className="bg-dark" as={Link} to={`/products`}>Back</Button>*/}

					      </Card.Body>
					</Card>

				</Col>
			</Row>
		</Container>

	)
}