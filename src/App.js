import './App.css';

import {useState, useEffect} from 'react';


import {UserProvider} from './UserContext';
import {ProductProvider} from './ProductContext';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';



import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';


import Home from './pages/Home';
import Error from './pages/Error';
import Products from './pages/Products';
import Login from './pages/Login';
import Logout from './pages/Logout';
import AdminDashboard from './pages/AdminDashboard';
import Register from './pages/Register';
import AddProduct from './pages/AddProduct'
import Update from './pages/Update'
import Cart from './pages/Cart'
import UserDetails from './pages/UserDetails'
import ChangePassword from './pages/ChangePassword'
import SetUserAsAdmin from './pages/SetUserAsAdmin'



import {Container} from 'react-bootstrap'

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })


  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      console.log(data);
      // User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          first: data.first,
          last: data.last,
          address: data.address,
          email: data.email,
          mobile: data.mobile,
          password: data.password

        })
        console.log(data.isAdmin)
        // User is logged out
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])  

  return (
    /*Fragments - common pattern in React.js for a component to return multiple elements*/
    <>
    {/*Initializes that dynamic routing will be involved*/}
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar/>
        <Container>
            <Routes>
              <Route path="/" element={<Home/>} />
              <Route path="/products" element={<Products/>} />
              <Route path="/products/:productId" element={<ProductView/>} />
              <Route path="/register" element={<Register/>} />
              <Route path="/login" element={<Login/>} />  
              <Route path="/logout" element={<Logout/>} />
              <Route path="/admin" element={<AdminDashboard/>} />
              <Route path="/addProduct" element={<AddProduct/>} />
              <Route path="/:productId" element={<Update/>} />
              <Route path="/cart" element={<Cart/>} />
              <Route path="/details" element={<UserDetails/>} />
              <Route path="/ChangePassword" element={<ChangePassword/>} />
              <Route path="/SetUserAsAdmin" element={<SetUserAsAdmin/>} />
        
              <Route path="/*" element={<Error/>} />  
          </Routes>
        </Container>
      </Router>
    </UserProvider>
    </>
  );

}

export default App;
