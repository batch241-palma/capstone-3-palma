import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home(){


	const data = {
		title: "Krusty Krabs Restaurant",
		content: "The best burger in town",
		destintion: "/",
		label: "Order Now!"
	}

	return(
		<>
			<Banner data={data} />
			<Highlights />
		</>
	)
}