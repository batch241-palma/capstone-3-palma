import { useState, useEffect, useContext } from 'react';

import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';

import {useParams, Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Update() {

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	// The "useParams" is a hook that allows us to retrieve the courseId passed via URL params
	const {productId} = useParams();
	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [isActive, setIsActive] = useState()
	const [save, setSave] = useState(false);
	const [state, setState] = useState(isActive)
	console.log(isActive)
	let newIsActive = isActive
	

  	const toggle = () => {
    setIsActive(!isActive);
  }

	const updateDetails = (e) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				'content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				name: name,
				description: description,
				price: price,
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
	
			if(data === true){
				Swal.fire({
	 			 title: "Successfully added",
	 			 icon: "success",
	 			 text: "You have successfully updated this product."
				})
				
			} else {
	 		 	Swal.fire({
		 		title: "Something went wrong",
		  		icon: "error",
		 		text: "Please try again."
				})
			}	


navigate("/products")
		})
	}

	// will retrieve the details of the course from our database to be displayed in the "CourseView" page
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setIsActive(data.isActive);
			console.log(data.isActive)
		})
	}, [productId])

    useEffect(() => {
        // Validation to enable submit button when all fields are populated.
        if(name !== '' && description !=='' && price !== ''){
            setSave(true);
        } else{
            setSave(false);
        }
    }, [name, description, price])	


	return (

        <Form onSubmit={updateDetails}>
            <Form.Group controlId="name">
                <Form.Label>Name</Form.Label>
                <Form.Control 
                    type="name" 
                    placeholder={name} 
                    value={name}
                    onChange={e => setName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="description">
                <Form.Label>Description</Form.Label>
                <Form.Control 
                    type="description" 
                    placeholder={description}
                    value={description}
                    onChange={e => setDescription(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="price">
                <Form.Label>Price</Form.Label>
                <Form.Control 
                    type="price" 
                    placeholder={price}
                    value={price}
                    onChange={e => setPrice(e.target.value)}
                    required
                />
            </Form.Group>      
            <br/>

          <div className = "d-flex flex-column" style={{gap: ".5rem"}}>
		  <div className="d-flex" style={{gap: "5rem"}}>

		  { isActive?
          <Button className="me-auto bg-danger" onClick={toggle}>
          {/*{isActive ? 'Active' : 'Inactive'}*/} Deactivate
          </Button>
          :
          <Button className="me-auto bg-primary" onClick={toggle}>
          {/*{isActive ? 'Active' : 'Inactive'}*/} Activate
          </Button>
      	  }
                      

             { save ?
                <Button variant="success" type="submit" id="submitBtn">
                Save Changes
                </Button>
            :
                <Button variant="success" type="submit" id="submitBtn" disabled>
                Save Changes
                </Button>
            }
            </div></div>
        </Form>





		// <Container>
		// 	<Row>
		// 		<Col lg={{span: 6, offset:3}} >
		// 			<Card>
		// 			      <Card.Body className="text-center">
		// 			        <Card.Title>{name}</Card.Title>
		// 			        <Card.Subtitle>Description:</Card.Subtitle>
		// 			        <Card.Text>{description}</Card.Text>
		// 			        <Card.Subtitle>Price:</Card.Subtitle>
		// 			        <Card.Text>PhP {price}</Card.Text>
		// 			        <Card.Subtitle>for added text</Card.Subtitle>
		// 			        <Card.Text>for added text</Card.Text>
					        
		// 			        {
		// 			        	(user.id !== null) ?
		// 			        		<Button variant="primary" onClick={() => enroll(productId)} >Add to cart</Button>

		// 			        		:
		// 			        		<Button className="btn btn-danger" as={Link} to="/login"  >Log in to Order</Button>
		// 			        }
		// 			        <br/>
		// 			        <Button className="bg-primary" as={Link} to={`/products`}>Back</Button>

		// 			      </Card.Body>
		// 			</Card>
		// 		</Col>
		// 	</Row>
		// </Container>

	)
}
