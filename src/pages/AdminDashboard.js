import {Card, Button, ToggleButton, Container, Row, Col} from 'react-bootstrap';

import {useParams, Link, useNavigate} from 'react-router-dom';



export default function AdminDashboard() {



return(
<Container>	
	<Row >
<Col xs={12} md={4} lg={4} className="g-3">
  <Card >
  <Card.Body className="text-center">
  <Card.Title>Retrieve List Of All Products</Card.Title>
	<div>
	<Button className="bg-primary" as={Link} to={`/products`}>
	Enter
	</Button>
	</div>

</Card.Body>
</Card >
</Col>


<Col xs={12} md={4} lg={4} className="g-3">
  <Card >
  <Card.Body className="text-center">
  <Card.Title>Create new product</Card.Title>
	<div>
	<Button className="bg-primary" as={Link} to={"/addProduct"}>
	Enter
	</Button>
	</div>

</Card.Body>
</Card >
</Col>

<Col xs={12} md={4} lg={4} className="g-3">
  <Card >
  <Card.Body className="text-center">
  <Card.Title>Retrieve All Users</Card.Title>
	<div>
	<Button className="bg-primary" as={Link} to={`/SetUserAsAdmin`}>
	Enter
	</Button>
	</div>

</Card.Body>
</Card >
</Col>

	</Row>
</Container>	
)
}