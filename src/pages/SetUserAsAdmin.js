import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Dropdown, DropdownButton, Form  } from 'react-bootstrap';

import {Navigate, Link, NavLink, useParams} from 'react-router-dom';

import UserCard from '../components/UserCard';

import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function SetUserAsAdmin() {
	const {user, setUser} = useContext(UserContext);
	// const {userId} = useParams();
	// const{admin, setAdmin} = useContext(user.isAdmin)
	const [isActive, setIsActive] = useState()
	const[allUsers, setAllUsers] = useState([]);

// const toggle = () => {
//     setIsActive(!isActive);
//   }

	// console.log(isActive)

	useEffect(() =>{

	fetch(`${process.env.REACT_APP_API_URL}/users/all`)
				.then(res => res.json())
		.then(data => {
			console.log(data)

			setAllUsers(data.map(allUser => {
				return(
					<UserCard key={user.id} allUser={allUser} />
				)
			}))
		})
},[])

	return(
		
		<>
		<Container>
<Row>{allUsers} </Row>
</Container>
      	  </>
		)
}