import {useState, useEffect, useContext} from 'react';

import {Row, Col, Container} from 'react-bootstrap';

import ProductCard from '../components/ProductCard';

import UserContext from '../UserContext';

export default function Products(){
	const {user, setUser} = useContext(UserContext);

	const[products, setProducts] = useState([]);

	console.log(user.isAdmin)
	useEffect(() =>{

		user.isAdmin?
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
				.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return(
					<ProductCard key={product.id} product={product} />
				)
			}))
		})

		:
		fetch(`${process.env.REACT_APP_API_URL}/products/active`)

		.then(res => res.json())
		.then(data => {
			console.log(data)

			setProducts(data.map(product => {
				return(
					<ProductCard key={product.id} product={product} />
				)
			}))
		})
	}, []);

	return (

		<>
		<Container>
		{(user.isAdmin)?
		<>	
		<h1 style={{textAlignVertical: "center",textAlign: "center",}}>Admin Access</h1><br/>  
		</>
		:
		<>	
		<h1 style={{textAlignVertical: "center",textAlign: "center",}}>Products</h1><br/> 	
		</>
		}

		<Row>{products} </Row>
		 

		</Container>
		</>
	)

}