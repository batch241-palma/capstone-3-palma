import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Dropdown, DropdownButton, Form  } from 'react-bootstrap';

import {Navigate, Link, NavLink} from 'react-router-dom';

import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function UserDetails() {

const {user, setUser} = useContext(UserContext);

            
        console.log(user)
		console.log(user.isAdmin)

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			method: "POST",
			headers: {
				'content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				userId: user.id,
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		
			

		})
	


        
    

	return(

<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-left">
					        <Card.Title>User Information</Card.Title>
					        <Card.Subtitle>Name: {user.first} {user.last}</Card.Subtitle>
					        <Card.Subtitle>Address: {user.address}</Card.Subtitle>
					        <Card.Subtitle>Mobile Number: {user.mobile}</Card.Subtitle>
					        <Card.Subtitle>Email Address: {user.email}</Card.Subtitle>
					        <Card.Subtitle>Password: {user.password}</Card.Subtitle>
					        
					        <Card.Subtitle>Admin: {user.isAdmin}</Card.Subtitle>
					        <Button as={NavLink} to="/changePassword" >Change Password</Button>

					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
)
}