// import { Form, Button  } from 'react-bootstrap';

import { useState, useEffect, useContext } from 'react';

import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';

import {useParams, Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function ChangePassword() {

const {user, setUser} = useContext(UserContext);

	const [password, setPassword] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

fetch(`${process.env.REACT_APP_API_URL}/users/changePassword`, {
            method: 'POST',
            headers: {
                
                Authorization: `Bearer ${localStorage.getItem('token')}`,
                'content-Type': 'application/json'
            },
            body: JSON.stringify({
            password: user.password
            })
        })
        .then(res => res.json())
        .then(data => {
        	console.log(data)
        })	


return (
  
    <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Previous Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>New Password</Form.Label>
        <Form.Control type="email" placeholder="New Password" />
        <Form.Text className="text-muted">
          We'll never share your password with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Retype New Password</Form.Label>
        <Form.Control type="email" placeholder="Retype New Password" />
        
      </Form.Group>      


      <Button variant="primary" type="submit">
        Submit
      </Button>
    </Form>
  
  )
}
